# TaskQueue

Task queue

## Introduction

nodejs task queue, a solution for high-concurrency peaking for requests, IO operations, or other asynchronous operations

## Debugging instructions

1. pnpm build
2. pnpm example
3. pnpm debug (debug source)

## Usage introduction

### Install dependencies

`npm install task-queue-lib`
or
`yarn add task-queue-lib`
or
`pnpm install task-queue-lib`

### Introduce

#### ESM

```javascript
import { TaskQueue } from "task-queue-lib";
```

#### CJS

```javascript
const { TaskQueue } = require("task-queue-lib");
```

#### in your browser

```html
<script src="./node_modules/task-queue-lib/dist/umd/index.js"></script>
<script>
  console.log(TaskQueue);
</script>
```

### Use

#### Slice length maxLen

```javascript
const taskQueue = new TaskQueue({ maxLen: 10 });
```

#### Creates a new queue

```javascript
const taskQueue = new TaskQueue({ maxLen: 10 }); `
`taskQueue.push([() => {}])
```

#### push as many functions as you have in a single queue

```javascript
taskQueue.push(syncFn.bind(null, "args"));
```

#### Subsequent operations are triggered when all the functions in a queue have finished executing

```javascript
taskQueue.push([() => {}]).then(console.log); // [ undefined ]
```

or

```javascript
taskQueue.on(taskQueue.defaultKey, console.log).push([() => {}]);
```

#### Queue index, grouped by the second parameter, asynchronous operations are completed in groups

```javascript
const fn = (params) => params;
taskQueue.push([fn.bind(this, "hello")], "task1");
taskQueue.push([fn.bind(this, "world")], "task1").then(console.log); // [ 'hello', 'world' ]
taskQueue.push([fn.bind(this, "world")], "task2").then(console.log); // [ 'world' ]
```

#### Removes the first three asynchronous functions

```javascript
taskQueue.unshift(3);
```

#### initializes the current queue

```javascript
taskQueue.clearQueue();
```

#### Queue stepping functions and events

When we split the push queue using maxLen, we can listen for the step events of the queue, for example, if the queue length is 10 and maxLen is 3, then four steps and events will be executed.

```javascript
// When the TaskQueue object is instantiated, the incoming function stepCb receives a message for each execution of the queue. In addition, by using the on method, you can listen for the queue's step events. The event name rule is' ${key}:${step} ', key is the second parameter of taskQueue.push, and step is the current queue's step value.

const stopFn = async () => {
  const stopQueue = new TaskQueue({
    maxLen: 1,
    stepCb: (data) => {
      // data Type Reference: IStepCbParams
      console.log(data);
    },
  });
  const key = "key1";
  const queue = [
    () => Promise.resolve("hello"),
    () => Promise.resolve("1"),
    () => Promise.resolve("stop"),
    () => Promise.resolve("2"),
    () => Promise.reject("3"),
  ];
  stopQueue.push(queue, "key1");
  for (let i = 1; i < queue.length + 1; i++) {
    stopQueue.on(key + `:${i}`, (data) => {
      const isStop = data.step === 3;
      console.log(isStop);
      if (isStop) stopQueue.clearQueue(); // Stop subsequent operations on the queue
    });
  }
};
stopFn();
```
